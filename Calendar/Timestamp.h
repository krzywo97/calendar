#pragma once

#include <stdbool.h>

typedef struct
{
	short year, month, day, hour, minutes, seconds;
} Timestamp;

Timestamp create_timestamp();
Timestamp create_date_only_timestamp();
bool is_timestamp_valid(Timestamp timestamp);
bool month_with_30_days(int month);
bool is_leap_year(int year);
char* timestamp_to_string(Timestamp timestamp);
char* timestamp_time_to_string(Timestamp timestamp);
bool are_times_in_timestamps_equal(Timestamp a, Timestamp b);
Timestamp increment_time(Timestamp original);
Timestamp decrement_time(Timestamp original);
Timestamp duration(Timestamp a, Timestamp b);
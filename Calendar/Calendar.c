#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "Calendar.h"
#include "Event.h"
#include "Timestamp.h"

EventNode *calendar_head = NULL, *today_head = NULL;
int today_size = 0;

void insert_event(Event event)
{
	if (!event.valid)
	{
		printf("Wydarzenie nie moze sie skonczyc zanim sie rozpocznie\r\n");
		return;
	}
	if (calendar_head == NULL)
	{
		calendar_head = malloc(sizeof(EventNode));
		if (calendar_head == NULL)
		{
			printf("Nie udalo sie zaalokowac pamieci!\r\n");
			return;
		}
		calendar_head->event = event;
		calendar_head->previous = NULL;
		calendar_head->next = NULL;
		return;
	}

	EventNode* current = calendar_head;

	while (current->next != NULL)
	{
		current = current->next;
	}

	current->next = malloc(sizeof(EventNode));
	if (current->next == NULL) return;
	current->next->previous = current;
	current->next->event = event;
	current->next->next = NULL;
}

Event get_event(int position)
{
	EventNode* current = calendar_head;
	int i = 0;
	while (i < position)
	{
		current = current->next;
	}

	return current->event;
}

void list_events()
{
	EventNode* current = calendar_head;
	int i = 0;
	while (current != NULL)
	{
		printf("%d. %s - %s\r\n", ++i, timestamp_to_string(current->event.start), timestamp_to_string(current->event.end));
		current = current->next;
	}
	if (i == 0)
	{
		printf("Nie masz zadnych zajec\r\n");
	}
}

void print_free_time(Timestamp day)
{
	printf("Wolne godziny w dniu dzisiejszym:\r\n");
	if (calendar_head == NULL)
	{
		printf("1. 00:00:00 - 23:59:59 (czas trwania: 23:59:59)\r\n");
		return;
	}

	EventNode *current = calendar_head;
	int counter = 0;
	while (current != NULL)
	{
		if (is_today_event(current->event, day))
		{
			insert_to_today_events(trim_event(current->event, day));
		}

		current = current->next;
	}

	if (today_head == NULL)
	{
		printf("1. 00:00:00 - 23:59:59 (czas trwania: 23:35:59)\r\n");
		return;
	}

	current = today_head;

	sort_today_events();
	if (!(are_times_in_timestamps_equal(current->event.start, (Timestamp) { 0, 0, 0, 0, 0, 0 })))
	{
		char* end_time = timestamp_time_to_string(decrement_time(current->event.start));
		printf("%d. 00:00:00 - %s (czas trwania: %s)\r\n", ++counter, end_time, end_time);
	}
	while (current != NULL)
	{
		if (current->next != NULL)
		{
			Timestamp start_time = increment_time(current->event.end);
			Timestamp end_time = decrement_time(current->next->event.start);
			char* dur = timestamp_time_to_string(duration(end_time, start_time));
			printf("%d. %s - %s (czas trwania: %s)\r\n", ++counter, timestamp_time_to_string(start_time), timestamp_time_to_string(end_time), dur);
		}
		else
		{
			if (!are_times_in_timestamps_equal(current->event.end, (Timestamp) { 0, 0, 0, 23, 59, 59 }))
			{
				Timestamp start_time = increment_time(current->event.end);
				char* dur = timestamp_time_to_string(duration(start_time, (Timestamp) { 0, 0, 0, 23, 59, 59 }));
				printf("%d. %s - 23:59:59 (czas trwania: %s)\r\n", ++counter, timestamp_time_to_string(start_time), dur);
			}
		}
		current = current->next;
	}

	if (counter == 0)
	{
		printf("Dzisiaj nie masz wolnego czasu :(\r\n");
	}

	clear_today_events();
}

void insert_to_today_events(Event event)
{
	if (today_head == NULL)
	{
		today_head = malloc(sizeof(EventNode));
		if (today_head == NULL)
		{
			printf("Nie udalo sie zaalokowac pamieci!\r\n");
			return;
		}
		today_head->event = event;
		today_head->previous = NULL;
		today_head->next = NULL;
		today_size++;
		return;
	}

	EventNode* current = today_head;

	while (current->next != NULL)
	{
		current = current->next;
	}

	current->next = malloc(sizeof(EventNode));
	if (current->next == NULL)
	{
		printf("Nie udalo sie zaalokowac pamieci!\r\n");
		return;
	}
	current->next->previous = current;
	current->next->event = event;
	current->next->next = NULL;
	today_size++;
}

bool is_today_event(Event event, Timestamp today)
{
	if (event.start.year < today.year)
	{
		if (event.end.year < today.year) return false;
		if (event.end.year > today.year) return true;

		if (event.end.year == today.year)
		{
			if (event.end.month < today.month) return false;
			if (event.end.month > today.month) return true;

			if (event.end.month == today.month)
			{
				if (event.end.day < today.day) return false;
				if (event.end.day >= today.day) return true;
			}
		}
	}

	if (event.start.year > today.year) return false;

	if (event.start.year == today.year)
	{
		if (event.end.year > today.year)
		{
			if (event.start.month > today.month) return false;
			if (event.start.month < today.month) return true;

			if (event.start.month == today.month)
			{
				if (event.start.day > today.day) return false;
				if (event.start.day <= today.day) return true;
			}
		}

		if (event.end.year == today.year)
		{
			if (event.start.month > today.month) return false;

			if (event.start.month < today.month)
			{
				if (event.end.month < today.month) return false;
				if (event.end.month > today.month) return true;
				
				if (event.end.month == today.month)
				{
					if (event.end.day < today.day) return false;
					if (event.end.day >= today.day) return true;
				}
			}

			if (event.start.month == today.month)
			{
				if (event.start.day > today.day) return false;
				if (event.start.day == today.day) return true;

				if (event.start.day < today.day)
				{
					if (event.end.month > today.month) return true;
					if (event.end.month == today.month)
					{
						if (event.end.day < today.day) return false;
						if (event.end.day >= today.day) return true;
					}
				}
			}
		}
	}

	return false;
}

Event trim_event(Event event, Timestamp today)
{
	if (event.start.year < today.year)
	{
		event = trim(event, today, true);
	}
	else if (event.start.year == today.year)
	{
		if (event.start.month < today.month)
		{
			event = trim(event, today, true);
		}
		else if (event.start.month == today.month
			&& event.start.day < today.day)
		{
			event = trim(event, today, true);
		}
	}

	if (event.end.year > today.year)
	{
		event = trim(event, today, false);
	}
	else if (event.end.year == today.year)
	{
		if (event.end.month > today.month)
		{
			event = trim(event, today, false);
		}
		else if (event.end.month == today.month
			&& event.end.day > today.day)
		{
			event = trim(event, today, false);
		}
	}

	return event;
}

Event trim(Event event, Timestamp today, bool start)
{
	Timestamp bound = (start ? event.start : event.end);
	bound.year = today.year;
	bound.month = today.month;
	bound.day = today.day;
	bound.hour = (start ? 0 : 23);
	bound.minutes = (start ? 0 : 59);
	bound.seconds = (start ? 0 : 59);
	if (start)
		event.start = bound;
	else
		event.end = bound;
	return event;
}

void sort_today_events()
{
	if (today_size < 2) return;
	EventNode *current = today_head, *swappable = today_head->next;
	while (current->next != NULL)
	{
		while (swappable != NULL)
		{
			if (is_later(current->event.start, swappable->event.start))
			{
				Event holder = current->event;
				current->event = swappable->event;
				swappable->event = holder;
			}
			swappable = swappable->next;
		}

		current = current->next;
		swappable = current->next;
	}
}

bool is_later(Timestamp what, Timestamp than)
{
	if (what.hour > than.hour) return true;
	if (what.hour < than.hour) return false;

	if (what.minutes > than.minutes) return true;
	if (what.minutes < than.minutes) return false;

	if (what.seconds > than.seconds) return true;
	if (what.seconds < than.seconds) return false;

	return false;
}

void clear_today_events()
{
	if (today_head == NULL) return;

	EventNode *current = today_head, *next;
	while (current != NULL)
	{
		next = current->next;
		free(current);
		current = next;
	}
	today_head = NULL;
}
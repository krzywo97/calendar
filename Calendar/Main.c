#include <stdio.h>

#include "Calendar.h"
#include "Event.h"
#include "Menu.h"
#include "Timestamp.h"
#include "Tests.h"

int main()
{
	int tests_results = run_tests();
	if (tests_results != 0) {
		printf("Tests failed! Exit code: %d\r\n", tests_results);
		return 1;
	}

	bool run = true;
	while (run)
	{
		run = show_menu();
	}

	return 0;
}
#pragma once

#include <stdbool.h>

#include "Event.h"

typedef struct EventNode EventNode;

struct EventNode {
	Event event;
	EventNode *previous, *next;
};

void insert_event(Event event);
void list_events();
void print_free_time(Timestamp day);
void insert_to_today_events(Event event);
bool is_today_event(Event event, Timestamp today);
Event trim_event(Event event, Timestamp today);
Event trim(Event event, Timestamp today, bool start);
void sort_today_events();
bool is_later(Timestamp what, Timestamp than);
void clear_today_events();
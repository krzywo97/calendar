#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "Calendar.h"
#include "Event.h"
#include "Menu.h"
#include "Timestamp.h"

bool show_menu()
{
	bool quit = false;
	printf("Kalendarz\r\na: Dodaj wydarzenie\r\ns: Pokaz wszystkie wydarzenia\r\nf: Pokaz wolny czas w danym dniu\r\nq: Wyjscie\r\nWybor: ");
	char choice = '\n';
	while (choice == '\n' || choice == EOF)
	{
		scanf("%c", &choice);
	}
	
	printf("\r\n");

	switch (choice)
	{
	case 'a':
		printf("Start wydarzenia:\r\n");
		Timestamp start = create_timestamp();
		printf("Koniec wydarzenia:\r\n");
		Timestamp end = create_timestamp();
		Event event = create_event(start, end);
		insert_event(event);
		break;
	case 's':
		list_events();
		break;
	case 'f':
		printf("Ktory dzien chcesz sprawdzic? ");
		Timestamp day = create_date_only_timestamp();
		print_free_time(day);
		break;
	case 'q':
		quit = true;
		break;
	default:
		printf("Nie wiem co masz na mysli :/\r\n");
	}
	printf("\r\n");
	return !quit;
}
#include <stdbool.h>
#include <string.h>

#include "Calendar.h"
#include "Event.h"
#include "Tests.h"
#include "Timestamp.h"

int run_tests()
{
	if (!test_timestamps()) return 1;
	if (!test_timestamp_to_string()) return 2;
	if (!test_is_today_event()) return 3;
	if (!test_trim_events()) return 4;
	if (!test_events()) return 5;
	return 0;
}

//run_tests() returns 1 if this method fails
bool test_timestamps()
{
	Timestamp test_cases[] = {
		//cases below should make function return true
		{1999, 1, 1, 12, 0 , 0},
		//cases below should make function return false
		{0, 0, 0, 0, 0, 0} //TODO: handle all possible cases
	};

	int cases = sizeof(test_cases) / sizeof(Timestamp);

	for (int i = 0; i < sizeof(test_cases); i++)
	{
		bool result = is_timestamp_valid(test_cases[i]);
		if (i == 0 && !result) return false;
		if (i != 0 && result) return false;
	}

	return true;
}

//run_tests() returns 2 if this method fails
bool test_timestamp_to_string()
{
	typedef struct {
		Timestamp timestamp;
		char expected[20];
	} TimestampToStringTestCase;

	TimestampToStringTestCase test_cases[] = {
		//cases below should make function return true
		{{2000, 1, 5, 12, 0, 5}, "05-01-2000 12:00:05"},
		//cases below should make function return false
		{{2000, 1, 5, 12, 0, 5}, "01-01-2000 12:00:05"} //TODO: handle all possible cases
	};

	int cases = sizeof(test_cases) / sizeof(TimestampToStringTestCase);

	for (int i = 0; i < cases; i++)
	{
		char* actual = timestamp_to_string(test_cases[i].timestamp);
		bool result = !strcmp(test_cases[i].expected, actual);
		if(i < 1 && !result) return false;
		if (i >= 1 && result) return false;
	}

	return true;
}

//run_tests() returns 3 if this method fails
bool test_is_today_event()
{
	typedef struct {
		Event event;
		Timestamp today;
	} IsTodayEventTestCase;

	IsTodayEventTestCase test_cases[] = {
		//cases below should make function return true
		{{true, {2000, 1, 1, 0, 0, 0}, {2000, 1, 1, 10, 0, 0}}, {2000, 1, 1}},
		{{true, {2000, 1, 1, 23, 59, 59}, {2000, 1, 2, 10, 0, 0}}, {2000, 1, 1}}
	};

	int cases = sizeof(test_cases) / sizeof(IsTodayEventTestCase);

	for (int i = 0; i < cases; i++)
	{
		bool result = is_today_event(test_cases[i].event, test_cases[i].today);
		if (i < 2 && !result) return false;
		if (i >= 2 && result) return false;
	}

	return true;
}

bool test_trim_events()
{
	Event test_cases[][2] = {
		//cases below should make function return true
		{{true, {2000, 1, 1, 0, 0, 0}, {2000, 1, 2, 10, 0, 0}}, {true, {2000, 1, 1, 0, 0, 0}, {2000, 1, 1, 23, 59, 59} }}
	};

	Timestamp today = { 2000, 1, 1, 0, 0, 0 };

	int cases = sizeof(test_cases) / sizeof(Event) / 2;
	for (int i = 0; i < cases; i++)
	{
		bool result = are_events_equal(trim_event(test_cases[i][0], today), test_cases[i][1]);
		if (i < 1 && !result) return false;
		if (i >= 1 && result) return false;
	}

	return true;
}

//run_tests() returns 4 if this method fails
bool test_events()
{
	Timestamp test_cases[][2] = {
		//cases below should make function return true
		{{1999, 1, 1, 12, 0, 0}, {2000, 1, 1, 12, 0, 0}},
		//cases below should make function return false
		{{1999, 1, 1, 12, 0, 0}, {1999, 1, 1, 12, 0, 0}},
		{{1999, 2, 1, 12, 0, 0}, {1999, 1, 1, 12, 0, 0}},
		{{2000, 1, 1, 12, 0, 0}, {1999, 1, 1, 12, 0, 0}} //TODO: handle all possible cases
	};

	int cases = sizeof(test_cases) / sizeof(Timestamp) / 2;

	for (int i = 0; i < cases; i++)
	{
		bool result = validate_duration(test_cases[i][0], test_cases[i][1]);
		if(i == 0 && !result) return false;
		if (i != 0 && result) return false;
	}

	return true;
}
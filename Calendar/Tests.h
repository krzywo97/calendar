#pragma once

#include <stdbool.h>

int run_tests();
bool test_timestamps();
bool test_timestamp_to_string();
bool test_is_today_event();
bool test_trim_events();
bool test_events();
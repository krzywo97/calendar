#include <stdbool.h>

#include "Event.h"


Event create_event(Timestamp start, Timestamp end)
{
	Event event = { true };
	if (!validate_duration(start, end))
	{
		event.valid = false;
	}

	event.start = start;
	event.end = end;

	return event;
}

bool validate_duration(Timestamp start, Timestamp end)
{
	if (end.year < start.year) return false;
	if (end.year > start.year) return true;

	if (end.month < start.month) return false;
	if (end.month > start.month) return true;

	if (end.day < start.day) return false;
	if (end.day > start.day) return true;

	if (end.hour < start.hour) return false;
	if (end.hour > start.hour) return true;
	if (end.minutes < start.minutes) return false;
	if (end.minutes > start.minutes) return true;
	return !(end.seconds <= start.minutes);
}

bool are_events_equal(Event a, Event b)
{
	return a.start.year == b.start.year
		&& a.start.month == b.start.month
		&& a.start.day == b.start.day
		&& a.start.hour == b.start.hour
		&& a.start.minutes == b.start.minutes
		&& a.start.seconds == b.start.seconds
		&& a.end.year == b.end.year
		&& a.end.month == b.end.month
		&& a.end.day == b.end.day
		&& a.end.hour == b.end.hour
		&& a.end.minutes == b.end.minutes
		&& a.end.seconds == b.end.seconds;
}
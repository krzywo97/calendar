#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

#include "Timestamp.h"

Timestamp create_timestamp()
{
	Timestamp timestamp = { 0, 0, 0, 0, 0, 0 };
	do
	{
		printf("Podaj date i godzine w formacie DD-MM-YYYY HH:mm:ss: ");
		scanf("%2hd-%2hd-%4hd %2hd:%2hd:%2hd",
			&timestamp.day,
			&timestamp.month,
			&timestamp.year,
			&timestamp.hour,
			&timestamp.minutes,
			&timestamp.seconds);
	} while (!is_timestamp_valid(timestamp));
	
	return timestamp;
}

Timestamp create_date_only_timestamp()
{
	Timestamp date = { 0, 0, 0, 0, 0, 0 };
	do
	{
		printf("Podaj date w formacie DD-MM-YYYY: ");
		scanf("%2hd-%2hd-%4hd", &date.day, &date.month, &date.year);
	} while (!is_timestamp_valid(date));

	return date;
}

bool is_timestamp_valid(Timestamp timestamp)
{
	if (timestamp.year <= 0) return false;
	if (timestamp.month < 0 || timestamp.month > 12) return false;
	if (timestamp.day < 0) return false;
	if (timestamp.day > 31) return false;
	if (timestamp.day > 30 && month_with_30_days(timestamp.month)) return false;
	if (timestamp.day > 29 && timestamp.month == 2 && is_leap_year(timestamp.year)) return false;
	if (timestamp.day > 28 && timestamp.month == 2 && !is_leap_year(timestamp.year)) return false;
	if (timestamp.hour < 0 || timestamp.hour > 23) return false;
	if (timestamp.minutes < 0 || timestamp.minutes > 59) return false;
	if (timestamp.seconds < 0 || timestamp.seconds > 59) return false;
	return true;
}

bool month_with_30_days(int month)
{
	return (month == 4)
		|| (month == 6)
		|| (month == 9)
		|| (month == 11);
}

bool is_leap_year(int year)
{
	return (year % 4 == 0)
		&& (year % 400 != 0);
}

char* timestamp_to_string(Timestamp timestamp)
{
	char* string = malloc(sizeof(char) * 20);
	if (string == NULL) return "01-01-1970 00:00:00";
	sprintf(string, "%02hd-%02hd-%04hd %02hd:%02hd:%02hd", timestamp.day, timestamp.month, timestamp.year, timestamp.hour, timestamp.minutes, timestamp.seconds);
	return string;
}

char* timestamp_time_to_string(Timestamp timestamp)
{
	char* time = malloc(sizeof(char) * 8);
	if (time == NULL) return "00:00:00";
	sprintf(time, "%02hd:%02hd:%02hd", timestamp.hour, timestamp.minutes, timestamp.seconds);
	return time;
}

bool are_times_in_timestamps_equal(Timestamp a, Timestamp b)
{
	return a.hour == b.hour
		&& a.minutes == b.minutes
		&& a.seconds == b.minutes;
}

Timestamp increment_time(Timestamp original)
{
	original.seconds = (original.seconds + 1) % 60;
	if (original.seconds == 0)
	{
		original.minutes = (original.minutes + 1) % 60;
		if (original.minutes == 0)
		{
			original.hour = (original.hour + 1) % 24;
			if (original.hour == 0)
			{
				original.hour = 23;
				original.minutes = original.seconds = 59;
			}
		}
	}
	return original;
}

Timestamp decrement_time(Timestamp original)
{
	original.seconds = (original.seconds - 1) % 60;
	if (original.seconds == -1)
	{
		original.seconds = 59;
		original.minutes = (original.minutes - 1) % 60;
		if (original.minutes == -1)
		{
			original.minutes = 59;
			original.hour = (original.hour - 1) % 24;
			if (original.hour == -1)
			{
				original.seconds = original.minutes = original.hour = 0;
			}
		}
	}
	return original;
}

/*
* here we assume timestamp b is the time when an event ends
* and timestamp a is the time an event starts
* b should be later than a and both should indicate the same day
* as we ignore dates
*/
Timestamp duration(Timestamp a, Timestamp b)
{
	a.hour = abs(b.hour - a.hour);
	a.minutes = abs(b.minutes - a.minutes);
	a.seconds = abs(b.seconds - a.seconds);
	return a;
}
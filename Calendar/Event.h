#pragma once

#include <stdbool.h>

#include "Timestamp.h"

typedef struct {
	bool valid;
	Timestamp start, end;
} Event;

Event create_event(Timestamp start, Timestamp end);
bool validate_duration(Timestamp start, Timestamp end);
bool are_events_equal(Event a, Event b);